# addy.io Sequoia PHP bindings

This crate requires PHP 8.

## Software requirements

The commands below are being run on Ubuntu 22.04, you may need to amend them if you are using a different version or distribution.

First install rust using rustup:

```bash
sudo apt-get install -y curl && curl https://sh.rustup.rs -sSf | sh -s -- -y
source $HOME/.cargo/env
```

Then install the following packages:

```bash
sudo apt install -y git wget make pkg-config nettle-dev libssl-dev capnproto libsqlite3-dev software-properties-common
```

Then add the following repositories:

```bash
wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key | sudo apt-key add -

echo "deb http://apt.llvm.org/jammy/ llvm-toolchain-jammy-18 main" | sudo tee /etc/apt/sources.list.d/llvm.list

sudo add-apt-repository ppa:ondrej/php

sudo apt update
```

Finally install clang and PHP 8:

```bash
sudo apt install -y clang-18 libclang1-18 libclang-18-dev php8.3 php8.3-dev
```

Clone the crate from GitLab:

```bash
cd ~

git clone https://gitlab.com/willbrowning/anonaddy-sequoia.git

cd anonaddy-sequoia
```

You should now be able to build the crate with:

```bash
cargo build --release
```

Run with:

```bash
php -dextension=$(pwd)/target/release/libanonaddy_sequoia.so -a
```

Run the tests with:

```bash
cargo test
```

## Available functions

### anonaddy_sequoia_encrypt

Encrypts a message and signs it:

```php
$signing_cert = file_get_contents("signing-key.asc");
$recipient_cert = file_get_contents("wiktor.asc");
echo anonaddy_sequoia_encrypt($signing_cert, $recipient_cert, "Hello Bruno!");
```

If using weak crypto the null policy can be enabled by passing an option:

```php
$signing_cert = file_get_contents("signing-key.asc");
$recipient_cert = file_get_contents("wiktor.asc");
echo anonaddy_sequoia_encrypt($signing_cert, $recipient_cert, "Hello Bruno!", ANONADDY_SEQUOIA_ALLOW_WEAK_CRYPTO);
```

If using SHA1 the standard policy can be modified to accept this by passing an option:

```php
$signing_cert = file_get_contents("signing-key.asc");
$recipient_cert = file_get_contents("sha1-key.asc");
echo anonaddy_sequoia_encrypt($signing_cert, $recipient_cert, "Hello Bruno!", ANONADDY_SEQUOIA_ALLOW_SHA1_CRYPTO);
```

Please see - [https://sequoia-pgp.org/blog/2023/02/01/202302-happy-sha1-day/](https://sequoia-pgp.org/blog/2023/02/01/202302-happy-sha1-day/) for more details on Sequoia’s deprecation of SHA1.

### anonaddy_sequoia_merge

Merges public information from a certificate:

```php
$current_cert = file_get_contents("wiktor-expired.asc");
$new_cert = file_get_contents("wiktor-fresh.asc");
echo anonaddy_sequoia_merge($current_cert, $new_cert);
```

### anonaddy_sequoia_minimize

Minimizes the certificate leaving only data necessary for `anonaddy_sequoia_encrypt`.

It is advisable to minimize certs that have been merged by `anonaddy_sequoia_merge` to
remove unnecessary data.

If using SHA1 the standard policy can be modified to accept this by passing `ANONADDY_SEQUOIA_ALLOW_SHA1_CRYPTO` as above.

```php
$full = file_get_contents("wiktor-full.asc");
echo strlen($full);
// 195709
$min = anonaddy_sequoia_minimize($full);
echo strlen($min);
// 3163
```

### anonaddy_sequoia_fingerprint

Return the fingerprint for a certificate:

```php
$cert = file_get_contents("wiktor.asc");
echo anonaddy_sequoia_fingerprint($cert);
// 653909A2F0E37C106F5FAF546C8857E0D8E8F074
```

## Credits

Thanks to [Wiktor](https://gitlab.com/wiktor) who did all the hard work putting these functions together.

Thanks to [David Cole](https://github.com/davidcole1340) for his awesome [ext-php-rs](https://github.com/davidcole1340/ext-php-rs) crate.
